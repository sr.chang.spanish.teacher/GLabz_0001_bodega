# GLabz_0001_bodega
/ESTE DOCUMENTO OMITE LAS TILDES Y ENIES PARA EVITAR PROBLEMAS DE COMPATIBILIDAD/ 
Este proyecto busca disenar e implementar un sistema automatizado de riego y monitoreo
para una carpa de 80x80x160.

Se propone el monitoreo en tiempo real de un sensor de humedad y temperatura ht11 y un
sensor de moisture del suelo

el sistem de riego consta de un tanque de agua, que se conecta desde la alimentacion de la casa 
y se controla con una solenoide. la salida de éste hacia el tanque de mezcla también se controla por una solenoide
y se tiene el tripack con 2 bombas por recipiente, una que dosifica y otra que homogeniza el contenido con burbujas.
luego la camara de mezcla tiene un solenoide que deja caer por gravedad el contenido hacia las plantas.

en esta etapa preliminar, se implementara el monitoreo de las tres variables y el control 
del riego con una HMI en QT, exportada para linux y android. con un grafico en tiempo real y
controles para activar o desactivar el riego.

